<?php

use App\Http\Controllers\api\v1\TransaksiController;
use App\Http\Controllers\api\v1\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::controller(UserController::class)->group(function () {
    Route::post('/v1/register', 'register');
    Route::post('/v1/login', 'login');
    Route::middleware('auth:sanctum')->get('/v1/saldo', 'saldo');
});

Route::middleware('auth:sanctum')->controller(TransaksiController::class)->group(function () {
    Route::post('/v1/transaksi', 'save');
    Route::get('/v1/transaksi/history', 'history');
});
