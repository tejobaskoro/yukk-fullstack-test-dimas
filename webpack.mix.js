const mix = require('laravel-mix');

mix
    .js('resources/vue/app.js', 'public/js')
    .vue({
        version: 3,
    })
    .sass('resources/sass/app.scss', 'public/css')
    .sass('resources/sass/login.scss', 'public/css')
    .options({
        processCssUrls: false,
        postCss: [require('autoprefixer')],
    });
;
