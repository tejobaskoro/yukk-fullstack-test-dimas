<?php

namespace App\Http\Controllers\api\v1;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    //

    function login(Request $request)
    {
        $p = $request->only('email', 'password');
        $this->loginValidator($p)->validate();

        if (Auth::attempt($p)) {
            $user = Auth::user();
            $user->token = $user->createToken('user')->plainTextToken;

            return response()->json(['valid' => true, 'data' => $user]);
        } else {
            return response()->json(['valid' => false, 'message' => 'Unauthorized']);
        }
    }

    public function register(Request $request)
    {
        $p = $request->all();
        $this->registerValidator($p)->validate();

        $user = User::create([
            'nama' => $p['nama'],
            'email' => $p['email'],
            'password' => Hash::make($p['password']),
            'saldo' => 0 # init saldo on register
        ]);
        $user->token = $user->createToken('user')->plainTextToken;

        return response()->json(['valid' => true, 'data' => $user]);
    }

    protected function loginValidator($data)
    {
        return Validator::make($data, [
            'email' => ['required', 'string'],
            'password' => ['required', 'string'],
        ]);
    }

    protected function registerValidator($data)
    {
        return Validator::make($data, [
            'nama' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8'],
        ]);
    }

    function saldo()
    {
        $user = auth('sanctum')->user();
        return response()->json(['valid' => true, 'data' => ['saldo' => $user->saldo]]);
    }

}
