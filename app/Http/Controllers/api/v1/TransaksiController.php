<?php

namespace App\Http\Controllers\api\v1;

use App\Http\Controllers\Controller;
use App\Models\Transaksi;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class TransaksiController extends Controller
{
    //

    function save(Request $request)
    {
        $p = $request->all();
        $this->validator($p)->validate();

        DB::beginTransaction();

        try {
            $data = [
                'id_user' => auth('sanctum')->user()->id,
                'guid' => bcrypt(date('YmdHis')),
                'tipe' => $p['tipe'],
                'jumlah' => $p['jumlah'],
                'keterangan' => $p['keterangan'],
            ];

            # Insert transaksi
            Transaksi::create($data);

            # Update balance
            $user = User::find($data['id_user']);

            if ($data['tipe'] == 'TOPUP') {
                $user->saldo = $user->saldo + $data['jumlah'];
            } else {
                $user->saldo = $user->saldo - $data['jumlah'];
            }

            $user->save();

            DB::commit();

            return response()->json(['valid' => true, 'data' => ['saldo' => $user->saldo]]);
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);

            return response()->json(['valid' => false]);
        }
    }

    protected function validator($data)
    {
        return Validator::make($data, [
            'tipe' => ['required', 'string'],
            'jumlah' => ['required', 'numeric'],
            'keterangan' => ['required', 'string'],
        ]);
    }

    function history(Request $request)
    {
        $p = $request->all();

        $user = auth('sanctum')->user();

        # pagination param
        $page = $p['page'];
        $per_page = 3;
        $offset = ($page - 1) * $per_page;

        $transaksi = Transaksi::where('id_user', $user->id);

        # Filter data
        # 1. tipe
        if (isset($p['tipe']) && $p['tipe'] !== 'SEMUA') {
            $transaksi = $transaksi->where('tipe', $p['tipe']);
        }

        # 2. tanggal transaksi
        if (isset($p['tanggal'])) {
            $transaksi = $transaksi->where(DB::raw('DATE(created_at)'), $p['tanggal']);
        }

        # 3. keterangan
        if (isset($p['keterangan'])) {
            $transaksi = $transaksi->where('keterangan', 'like', '%'.$p['keterangan'].'%');
        }

        $total_data = $transaksi->count();
        $total_halaman = ceil($total_data / $per_page);

        $transaksi = $transaksi->orderByDesc('id')
            ->offset($offset)->limit($per_page)
            ->get();

        return response()->json([
            'valid' => true,
            'data' => $transaksi,
            'total_data' => $total_data,
            'total_halaman' => $total_halaman
        ]);
    }
}
