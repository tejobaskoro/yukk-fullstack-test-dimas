import {createRouter, createWebHistory} from "vue-router";
import store from './../store'

import Dashboard from './../pages/Dashboard'

const routes = [
  {
    path: '/',
    name: 'Dashboard',
    component: Dashboard,
    meta: {requiresAuth: true},
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import(/* webpackChunkName: "login" */ '../pages/Login.vue'),
  },
  {
    path: '/transaksi',
    name: 'Transaksi',
    component: () => import(/* webpackChunkName: "transaksi" */ '../pages/TransaksiForm.vue'),
    meta: {requiresAuth: true},
  },
  {
    path: '/transaksi/:id',
    name: 'Tambah / Ubah Transaksi',
    component: () => import(/* webpackChunkName: "transaksi-form" */ '../pages/TransaksiForm.vue'),
    meta: {requiresAuth: true},
  },
  {
    path: '/history',
    name: 'History Transaksi',
    component: () => import(/* webpackChunkName: "history" */ '../pages/History.vue'),
    meta: {requiresAuth: true},
  }
]

const router = createRouter({
  history: createWebHistory(),
  routes
});

router.beforeEach((to, from, next) => {
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    if (!store.getters['auth/isAuthenticated']) {
      next({name: 'Login'});
    } else {
      next();
    }
  } else {
    if (to.name === 'Login' && store.getters['auth/isAuthenticated']) {
      next({ name: 'Dashboard' });
    } else {
      next();
    }
  }
})

export default router
