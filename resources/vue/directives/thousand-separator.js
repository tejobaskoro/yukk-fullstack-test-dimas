export default {
  updated: (el) => {
    let value = parseInt(el.value.replace(/[^0-9]+/g, ""));

    if (!isNaN(value)) {
      el.value = value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    }
  },
  beforeMount: (el) => {
    el.addEventListener('keyup', (e) => {
      el.value = el.value.replace(/[^\d.]/g, '');
    })
  }
};
