import './bootstrap';

import {createApp} from 'vue';
import ToastPlugin from 'vue-toast-notification';
import 'vue-toast-notification/dist/theme-bootstrap.css';
import mitt from 'mitt';

import App from './App.vue';
import router from './router'
import store from './store'
import thousandSeparator from './directives/thousand-separator';

const emitter = mitt();
const app = createApp(App);

app.use(router);
app.use(store)
app.use(ToastPlugin)
app.directive('thousand-separator', thousandSeparator)

app.config.globalProperties.emitter = emitter;
app.config.globalProperties.$filters = {
  number(value) {
    if (value || value == 0)
      return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')
    else
      return ''
  }
}

app.mount('#app');
