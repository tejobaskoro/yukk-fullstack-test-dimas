import axios from 'axios';
import store from './../store';

import router from './../router'

const baseUrl = 'http://localhost:8000/api';
const clientToken = store.getters['auth/user'].token || '';

export const apiPost = (url, payload) => {
  payload._token = window.csrf;

  let auth = {
    headers: { 'Authorization': `Bearer ${clientToken}` }
  }
  if (url == '/v1/login' || url == '/v1/register') {
    auth = {}
  }

  return axios.post(baseUrl + url, payload, auth).then(response => {
    return response.data;
  }).catch(e => {
    if (e.response.status == 401) {
      router.push('/login?mesage=Unauthorized')
    }
  });;
}

export const apiGet = (url) => {
  return axios.get(baseUrl + url, {
    headers: { 'Authorization': `Bearer ${clientToken}` }
  }).then(response => {
    return response.data;
  }).catch(e => {
    if (e.response.status == 401) {
      router.push('/login?mesage=Unauthorized')
    }
  });
}

export const rawGet = (url) => {
  return axios.get(url, {
    mode: "no-cors",
    headers: {
      "Content-Type": "application/json",
    },
  }).then(response => {
    return response.data;
  });
}
