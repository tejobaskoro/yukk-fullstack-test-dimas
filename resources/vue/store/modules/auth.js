import Cookies from 'js-cookie';

const state = () => ({
  user: JSON.parse(Cookies.get('user') || '{}')
})

const mutations = {
  setUser(state, user) {
    state.user = user;
  },
  logout(state) {
    state.user = {};
  },
}

const actions = {
  login({ commit }, user) {
    commit('setUser', user);
    Cookies.set('user', JSON.stringify(user));
  },
  logout({ commit }) {
    commit('logout');
    Cookies.remove('user');
  },
}

const getters = {
  isAuthenticated: (state) => !!state.user.id,
  user: (state) => state.user
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
