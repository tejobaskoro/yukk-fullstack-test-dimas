const state = () => ({
  activePage: '',
  login: {},
})

const mutations = {
  setActivePage(state, val) {
    state.activePage = val
  },
  setLogin(state, val) {
    state.isLogin = true
    state.login = val
  },
}

export default {
  namespaced: true,
  state,
  mutations
}
