import { createStore, createLogger } from 'vuex'
import auth from './modules/auth'
import setting from './setting'

const debug = process.env.NODE_ENV !== 'production'

export default createStore({
  modules: {
    auth,
    setting
  },
  strict: debug,
  plugins: debug ? [createLogger()] : []
})
