@extends('layout/login_layout')

@section('content')
<div class="login">
    <div class="container">
        <div>
            <img src="https://yukk.co.id/images/YUKK.png" alt="Logo">
        </div>
        <div>
            <form method="POST" action="{{ route('register') }}">
                <h2>Login</h2>
                @csrf
                <div class="form-group">
                    <label>Nama</label>
                    <input type="text">
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input type="email">
                </div>
                <div class="form-group">
                    <label>Password</label>
                    <input type="password">
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn--primary">
                        LOGIN
                    </button>
                    <a href="{{ route('register') }}">
                        Belum punya akun? Daftar
                    </a>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
