@extends('layout/login_layout')

@section('content')
<div class="login">
    <div class="container">
        <div>
            <img src="https://yukk.co.id/images/YUKK.png" alt="Logo">
        </div>
        <div>
            <form method="POST" action="{{ route('register') }}">
                <h2>Daftar Baru</h2>
                @csrf
                <div class="form-group">
                    <label>Nama</label>
                    <input type="text" name="nama">
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input type="email" name="email">
                </div>
                <div class="form-group">
                    <label>Password</label>
                    <div class="input-password">
                        <input type="password" name="password" class="password-input">
                        <span class="material-symbols-outlined password-reveal">
                            visibility_off
                        </span>
                    </div>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn--primary">
                        DAFTAR
                    </button>
                    <a href="{{ route('login') }}">
                        Sudah punya akun? Login
                    </a>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
